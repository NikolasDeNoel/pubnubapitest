﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {           
            var root = "https://ps3.pubnub.com";
            var client = new HttpClient();
            client.BaseAddress = new Uri(root);
            var url = "/publish/pub-c-a52d65a3-cba0-4c8f-9ca7-59f838d0e2d3/sub-c-30cc9fb0-b2ff-11e5-b4bd-02ee2ddab7fe/0/NHC_CMD.55110/0/%7B%22id%22%3A%2255110%22%2C%22auth_key%22%3A%22e632c94f-3bd1-4d9c-a230-95788e85902c%22%2C%22service%22%3A%22NHC%22%2C%22payload%22%3A%7B%22correlation_id%22%3A%22gateway_console_correlation_id%22%2C%22data%22%3A%22%7B%5C%22cmd%5C%22%3A%5C%22systeminfo%5C%22%7D%22%7D%7D?uuid=d4e9097d-e76c-49aa-a6fc-aad9cdd0852f&auth=e632c94f-3bd1-4d9c-a230-95788e85902c&pnsdk=PubNub-JS-Web%2F3.7.20";
            //client.DefaultRequestHeaders.Add("Accept", "*/*");
            //client.DefaultRequestHeaders.Add("DNT", "1");
            //client.DefaultRequestHeaders.Add("Authorization", "Basic dGVzdF9nd19oYW1fbGFuQGZwdGwuYmU6VGVzdDEyMy4=");
            //client.DefaultRequestHeaders.Add("Origin", "https://s-nrt.fifthplay.com");
            //client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
            //client.DefaultRequestHeaders.Add("Referer", "https://s-nrt.fifthplay.com/Help/Console");
            //client.DefaultRequestHeaders.Add("Host", "ps3.pubnub.com");
            var result = client.GetAsync(url).GetAwaiter().GetResult();            
            var status = result.StatusCode;
            var response = result.Content.ReadAsByteArrayAsync().GetAwaiter().GetResult();
            string r = Encoding.UTF8.GetString(response, 0, response.Length);
        }
    }
}
